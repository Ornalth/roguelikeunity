




























//READ PCGWIKIDOT.com
//To add square, need to add cost for looking over square, effect, and picture, EVENT, adding over walking the square
//ROGUEBASIN
//PCG.com/wikidot or osmehting

public class Board
{

	GameState state;
	public GameStateDetails stateDetails;

	private static Board INSTANCE;

	private static readonly int separatorDistance = 2;

	public static readonly string PLAYER_INVENTORY_OFFERS = "PLAYERINVENTORYOFF";
	public static readonly string PLAYER_SELL_OFFERS = "PLAYER SELL OFERs";
	public static readonly string SHOW_TEXT = "SHOWNPCTEXT"; // string
	public static readonly string NPC_SELL_OFFERS = "SELLOFFERS";// List<Offer>
	public static readonly string NPC_INVENTORY_OFFERS = "NPC INVENTORY OFFERs"; // <List<Offer>
	public static readonly string SHOW_TILE_ITEMS = "SHOWTILEIMAGE"; // List<Item>
	public static readonly string SHOW_TILE_ITEMS_UPDATE = "SHOWTILEIMAGEUPDATE"; // List<Item>
	public static readonly string NPC_INVENTORY_OFFERS_UPDATE = "NPC INVENTORY UPDATE";
	public static readonly string PLAYER_INVENTORY_OFFERS_UPDATE = "PLAYER INVENTORY UPDATE";
	public static readonly string PLAYER_SELL_OFFERS_UPDATE = "PLAYER SELL UPDATE";
	public static readonly string NPC_SELL_OFFERS_UPDATE = "NPC SELL UPDATE";
	public static readonly string GAME_STATE_UPDATE = "GAMESTATE UDAPTE)";
	public static readonly string ITEM_OPTIONS = "ITEM_OPTINS"; // Arraylist<ItemOption>
	public static readonly string NPC_OPTIONS = "NPC OPTIONS";
	public static readonly string HIDE_OPTIONS = "HIDE ITEM OPTINS";

	public static readonly string SHOW_ERROR_DIALOG = "SHOWERROR DIALOG";
	public static readonly string REQUEST_INTEGER_VALUE = "GIMMEMONEY";

	int PERCENT_ENEMY_BEGIN = 80;
	int PERCENT_NPC_BEGIN = 80;
	int CITY_DUNGEON_BEGIN_INSET = 3;
	int numEnemies = 20;
	int numEvents = 20;
	int numItems = 20;
	int NUM_CITIES_BEGIN = 3;
	int NUM_DUNGEON_BEGIN = 3;
	int NUM_NPC_BEGIN = 5;
	int NUM_ENEMIES_BEGIN = 10;
	int NUM_ITEMS_BEGIN = 10;
	int NUM_EventS_BEGIN = 10;
	int FAILURE_STOP = 500;
	int day;
	private static bool debug = true;

	string[,] map = new string[Constants.height,Constants.width];
	string[,] temp = new string[Constants.height,Constants.width];
	Random rand = new Random();
	Tile[,] board = new Tile[Constants.height,Constants.width];
	Player player;
	public bool gameover = false;
	List<Enemy> enemies = new List<Enemy>();
	List<EVENT> Events = new List<EVENT>();
	List<NPC> npcs = new List<NPC>();
	Set<BoardObs> observers = new HashSet<BoardObs>();

	// "." == EMPTY SPOT || ENEMY SPOT || ITEM SPOT ANYTHING YOU WANT.
	// "W" == WALL
	// "C" == NPC SPOT || ITEM SPOT
	// "N" == NPC SPOT
	// "D" == ENEMY SPOT || ITEM SPOT
	// "E" == ENEMY
	// "X" == MUST BE BLANK (DOORS)

	// "V" == EventS
	// "E" == ENEMY
	// "N" == NPC SPOT
	// "P" == PLAYER
	// "I" == ITEM

	private int LARGE_CITY_WIDTH = 11;
	private int LARGE_CITY_HEIGHT = 11;

	private int SMALL_CITY_WIDTH = 7;
	private int SMALL_CITY_HEIGHT = 5;

	private int MED_CITY_WIDTH = 9;
	private int MED_CITY_HEIGHT = 7;

	private int LARGE_DUNGEON_WIDTH = 11;
	private int LARGE_DUNGEON_HEIGHT = 11;

	private int SMALL_DUNGEON_WIDTH = 7;
	private int SMALL_DUNGEON_HEIGHT = 5;

	private int MED_DUNGEON_WIDTH = 9;
	private int MED_DUNGEON_HEIGHT = 7;

	private void makeTownMaze(string[][] maze, int numCities, int numDungeons)
	{

		int failures = 0;
		for (int row = 0; row < Constants.height; row++)
		{
			for (int col = 0; col < Constants.width; col++)
			{
				maze[row][col] = ".";
			}
		}

		for (int i = 0; i < numCities; i++)
		{
			while (true)
			{
				int height = rand.nextInt(2) + 3;
				int width = rand.nextInt(5) + 3;
				switch (rand.nextInt(3))
				{
					case 0:
						height = LARGE_CITY_HEIGHT;
						width = LARGE_CITY_WIDTH;
						break;
					case 1:
						height = MED_CITY_HEIGHT;
						width = MED_CITY_WIDTH;
						break;
					case 2:
						height = SMALL_CITY_HEIGHT;
						width = SMALL_CITY_WIDTH;
						break;
				}

				int beginRow = rand.nextInt(Constants.height
						- CITY_DUNGEON_BEGIN_INSET)
						+ CITY_DUNGEON_BEGIN_INSET;
				int beginCol = rand.nextInt(Constants.width
						- CITY_DUNGEON_BEGIN_INSET)
						+ CITY_DUNGEON_BEGIN_INSET;
				bool isEmpty = true;
				if (beginRow + height + separatorDistance < Constants.height
						&& beginCol + width + separatorDistance < Constants.width
						&& beginRow - separatorDistance >= 0
						&& beginCol - separatorDistance >= 0)
				{
					for (int row = beginRow - separatorDistance; row < beginRow
							+ height + separatorDistance; row++)
					{
						for (int col = beginCol - separatorDistance; col < beginCol
								+ width + separatorDistance; col++)
						{
							if (!maze[row][col].equals("."))
							{
								isEmpty = false;
								break;
							}
						}
					}
					if (isEmpty)
					{
						List<Position> potentialDoors = new List<Position>();
						int numDoors = rand.nextInt(3) + 1;
						for (int row = beginRow; row < beginRow + height; row++)
						{
							for (int col = beginCol; col < beginCol + width; col++)
							{
								if (row == beginRow || col == beginCol
										|| row == beginRow + height - 1
										|| col == beginCol + width - 1)
								{
									maze[row][col] = "W";
									potentialDoors.Add(new Position(row, col));
								}
								else
								{
									maze[row][col] = "C";
									if (rand.nextInt(100) > PERCENT_NPC_BEGIN)
										maze[row][col] = "N";
								}
							}
						}
						for (int n = 0; n < numDoors; n++)
						{
							int slot = rand.nextInt(potentialDoors.Count);
							Position door = potentialDoors.get(slot);
							potentialDoors.remove(slot);
							maze[door.row][door.col] = "X";
						}
						break;
					}
					else
					{
						failures++;
						if (failures >= FAILURE_STOP)
							break;
					}
				}
			}
		}

		for (int i = 0; i < numDungeons; i++)
		{
			while (true)
			{
				int height = rand.nextInt(2) + 3;
				int width = rand.nextInt(5) + 3;
				switch (rand.nextInt(3))
				{
					case 0:
						height = LARGE_DUNGEON_HEIGHT;
						width = LARGE_DUNGEON_WIDTH;
						break;
					case 1:
						height = MED_DUNGEON_HEIGHT;
						width = MED_DUNGEON_WIDTH;
						break;
					case 2:
						height = SMALL_DUNGEON_HEIGHT;
						width = SMALL_DUNGEON_WIDTH;
						break;
				}
				int beginRow = rand.nextInt(Constants.height
						- CITY_DUNGEON_BEGIN_INSET)
						+ CITY_DUNGEON_BEGIN_INSET;
				int beginCol = rand.nextInt(Constants.width
						- CITY_DUNGEON_BEGIN_INSET)
						+ CITY_DUNGEON_BEGIN_INSET;
				bool isEmpty = true;
				if (beginRow + height + separatorDistance < Constants.height
						&& beginCol + width + separatorDistance < Constants.width
						&& beginRow - separatorDistance >= 0
						&& beginCol - separatorDistance >= 0)
				{
					for (int row = beginRow - separatorDistance; row < beginRow
							+ height + separatorDistance; row++)
					{
						for (int col = beginCol - separatorDistance; col < beginCol
								+ width + separatorDistance; col++)
						{
							if (!maze[row][col].equals("."))
							{
								isEmpty = false;
								break;
							}
						}
					}
					if (isEmpty)
					{
						List<Position> potentialDoors = new List<Position>();
						int numDoors = rand.nextInt(3) + 1;
						for (int row = beginRow; row < beginRow + height; row++)
						{
							for (int col = beginCol; col < beginCol + width; col++)
							{
								if (row == beginRow || col == beginCol
										|| row == beginRow + height - 1
										|| col == beginCol + width - 1)
								{
									maze[row][col] = "W";
									potentialDoors.Add(new Position(row, col));
								}
								else
								{
									maze[row][col] = "D";
									if (rand.nextInt(100) > PERCENT_ENEMY_BEGIN)
										maze[row][col] = "E";
								}
							}
						}
						for (int n = 0; n < numDoors; n++)
						{
							int slot = rand.nextInt(potentialDoors.Count);
							Position door = potentialDoors.get(slot);
							potentialDoors.remove(slot);
							maze[door.row][door.col] = "X";
						}
						break;
					}
					else
					{
						failures++;
						if (failures >= FAILURE_STOP)
							break;
					}
				}
			}
		}
		if (failures >= FAILURE_STOP)
		{
			Debug.Log("FAILED MAP - REDO");
			makeTownMaze(maze, numCities, numDungeons);
		}
	}

	// "." == EMPTY SPOT || ENEMY SPOT || ITEM SPOT ANYTHING YOU WANT.
	// "W" == WALL
	// "C" == NPC SPOT || ITEM SPOT

	// "D" == ENEMY SPOT || ITEM SPOT

	// "X" == MUST BE BLANK (DOORS)

	// "V" == EventS
	// "E" == ENEMY
	// "N" == NPC SPOT
	// "P" == PLAYER
	// "I" == ITEM

	private void addAdditional(string[][] maze, int numNPC, int numEnemies,
			int numItems, int numEvents)
	{
		while (numNPC > 0 || numEnemies > 0 || numItems > 0 || numEvents > 0)
		{
			int row = rand.nextInt(Constants.height);
			int col = rand.nextInt(Constants.width);

			if (maze[row][col].equals("."))
			{
				if (numEvents > 0)
				{
					maze[row][col] = "V";
					numEvents--;
				}
				else if (numNPC > 0)
				{
					maze[row][col] = "N";
					numNPC--;
				}
				else if (numEnemies > 0)
				{
					maze[row][col] = "E";
					numEnemies--;
				}
				else if (numItems > 0)
				{
					maze[row][col] = "I";
					numItems--;
				}
			}
			else if (maze[row][col].equals("C"))
			{
				if (numEvents > 0)
				{
					maze[row][col] = "V";
					numEvents--;
				}
				else if (numNPC > 0)
				{
					maze[row][col] = "N";
					numNPC--;
				}
				else if (numItems > 0)
				{
					maze[row][col] = "I";
					numItems--;
				}
			}
			else if (maze[row][col].equals("D"))
			{
				if (numEvents > 0)
				{
					maze[row][col] = "V";
					numEvents--;
				}
				else if (numEnemies > 0)
				{
					maze[row][col] = "E";
					numEnemies--;
				}
				else if (numItems > 0)
				{
					maze[row][col] = "I";
					numItems--;
				}
			}
		}
	}

	private void outputMaze(string[][] maze)
	{
		Debug.Log(" ");
		for (int col = 0; col < Constants.width; col++)
			Debug.Log(col % 10);
		Debug.Log("\n");
		for (int row = 0; row < Constants.height; row++)
		{
			Debug.Log(row % 10);
			for (int col = 0; col < Constants.width; col++)
			{
				Debug.Log(maze[row][col]);
			}
			Debug.Log("\n");
		}
		Debug.Log("\n");
	}

	private Board()
	{

	}

	public static Board getInstance()
	{
		if (INSTANCE == null)
		{
			INSTANCE = new Board();
		}

		return INSTANCE;
	}

	public void newGame()
	{
		makeTownMaze(map, NUM_CITIES_BEGIN, NUM_DUNGEON_BEGIN);

		convertToRegularTileSystem();
		addAdditional(map, NUM_NPC_BEGIN, NUM_ENEMIES_BEGIN, NUM_ITEMS_BEGIN,
				NUM_EventS_BEGIN);
		map[5][5] = "P";
		outputMaze(map);
		addToRegularTileSystem();
		setGameState(GameState.FREE);
		stateDetails = new GameStateDetails();
	}

	public void convertToRegularTileSystem()
	{
		board = new Tile[Constants.height,Constants.width];

		for (int row = 0; row < Constants.height; row++)
		{
			for (int col = 0; col < Constants.width; col++)
			{
				Tile tile = TileFactory.createTile(map[row][col].charAt(0),
						true, row, col);
				board[row][col] = tile;

			}
		}

		// addStuffToBoard(Stuff.ENEMY, numEnemies);
		// addStuffToBoard(Stuff.EVENT, numEvents);
		// addStuffToBoard(Stuff.ITEMS, numItems);
		// addStuffToBoard(Stuff.PLAYER, 1);
	}

	// "V" == EventS
	// "E" == ENEMY
	// "N" == NPC SPOT
	// "P" == PLAYER
	// "I" == ITEM
	private void addToRegularTileSystem()
	{
		// new Tile[Constants.height][Constants.width];

		for (int row = 0; row < Constants.height; row++)
		{
			for (int col = 0; col < Constants.width; col++)
			{
				Tile tile = board[row][col];
				switch (map[row][col].charAt(0))
				{
					case 'E':
						Enemy enemy = EnemyFactory.createEnemy(new Position(
								row, col));
						enemies.Add(enemy);
						tile.setUnit(enemy);
						break;
					case 'V':
						EVENT EVENT = EventFactory.createEvent(new Position(
								row, col));
						Events.Add(EVENT);
						tile.setEvent(EVENT);
						break;
					case 'I':
						tile.addItem(ItemFactory.createItem());
						break;
					case 'N':
						NPC npc = NPCFactory.createNPC(new Position(row, col));
						npcs.Add(npc);
						tile.setUnit(npc);
						break;
					case 'P':
						player = new Player();
						player.setRow(row);
						player.setCol(col);
						tile.setUnit(player);
						break;
				}
			}
		}

	}

	public bool isGameOver()
	{
		return gameover;
	}

	private void gameOver()
	{
		Debug.Log("GameOver");
		gameover = true;
	}

	enum Stuff {ENEMY, PLAYER, ITEMS, EVENT};

	public class Direction
	{
		public static readonly int NORTH = 0;
		public static readonly int NORTHEAST = 1;
		public static readonly int NORTHWEST = 2;
		public static readonly int EAST =3;
		public static readonly int SOUTH = 4;
		public static readonly int SOUTHEAST = 5;
		public static readonly int SOUTHWEST = 6;
		public static readonly int WEST = 7;
		public static readonly int NONE = 99;
	
		private static readonly int SIZE = 8;
		private static readonly Random rand = new Random();

		public static Direction randomDirection()
		{
			return rand.nextInt(SIZE);
		}

		public static int getChangeX(Direction d)
		{
			int changeX = 0;
			switch (d)
			{
				case EAST:
					changeX = 1;
					break;
				case NORTHEAST:
					changeX = 1;
					break;

				case SOUTHEAST:
					changeX = 1;
					break;
				case NORTHWEST:
					changeX = -1;
					break;
				case SOUTHWEST:
					changeX = -1;
					break;
				case WEST:
					changeX = -1;
					break;
				default:
					break;
			}
			return changeX;
		}

		public static int getChangeY(Direction d)
		{
			int changeY = 0;
			switch (d)
			{

				case NORTH:
					changeY = 1;
					break;
				case NORTHEAST:
					changeY = 1;
					break;
				case NORTHWEST:
					changeY = 1;
					break;
				case SOUTH:
					changeY = -1;
					break;
				case SOUTHEAST:

					changeY = -1;
					break;
				case SOUTHWEST:

					changeY = -1;
					break;

				default:
					break;
			}
			return changeY;
		}
	}

	public void interact(int index)
	{
		Tile tile = getPlayerTile();
		EVENT EVENT = tile.getEvent();
		if (EVENT != null)
		{
			// TODO!
		}
	}

	public void itemSelected(Item item2)
	{
		if (state == GameState.EXAMINE_GROUND)
		{
			showOptionsForItem(item2, ItemState.GROUND);
		}
		else if (state == GameState.EXAMINE_INVENTORY)
		{
			showOptionsForItem(item2, ItemState.INVENTORY);
		}
	}

	public void offerSelected(TradePaneEnum pane, Offer offer)
	{

		// ADD TO BUY LIST
		if (pane == TradePaneEnum.NPC_INVENTORY)
		{
			int index = stateDetails.npcOffers.indexOf(offer);
			if (offer.item is StackableItem)
			{
				StackableItem item = (StackableItem) offer.item;
				OfferItemUtils.addOffer(stateDetails.npcSellOffers, offer);
				if (item.getStackAmount() <= 0)
				{
					stateDetails.npcOffers.remove(index);
				}
			}
			else
			{
				stateDetails.npcSellOffers.Add(offer);
				stateDetails.npcOffers.remove(index);
			}
			stateDetails.weight += offer.item.getWeight();
			stateDetails.currentGold -= offer.value;
			// Offer offer = stateDetails.npcOffers.get(index);

			notifyObservers(stateDetails.npcOffers, NPC_INVENTORY_OFFERS_UPDATE);
			notifyObservers(stateDetails.npcSellOffers, NPC_SELL_OFFERS_UPDATE);
			// showOptionsForItem(offers.get(index).item,
			// ItemState.NPC_INVENTORY);
		}
		// REMOVE FROM BUY LIST
		else if (pane == TradePaneEnum.NPC_SELL)
		{
			int index = stateDetails.npcSellOffers.indexOf(offer);
			if (offer.item is StackableItem)
			{
				StackableItem item = (StackableItem) offer.item;
				OfferItemUtils.addOffer(stateDetails.npcOffers, offer);
				if (item.getStackAmount() <= 0)
				{
					stateDetails.npcSellOffers.remove(index);
				}
			}
			else
			{
				stateDetails.npcOffers.Add(offer);
				stateDetails.npcSellOffers.remove(index);
			}
			stateDetails.weight -= offer.item.getWeight();
			stateDetails.currentGold += offer.value;

			// stateDetails.npcOffers.Add(offer);
			// stateDetails.npcSellOffers.remove(index);
			// stateDetails.currentGold += offer.value;
			notifyObservers(stateDetails.npcOffers, NPC_INVENTORY_OFFERS_UPDATE);
			notifyObservers(stateDetails.npcSellOffers, NPC_SELL_OFFERS_UPDATE);
		}
		// ADD TO SELL LIST
		else if (pane == TradePaneEnum.PLAYER_INVENTORY)
		{
			int index = stateDetails.playerOffers.indexOf(offer);
			if (offer.item is StackableItem)
			{
				StackableItem item = (StackableItem) offer.item;
				OfferItemUtils.addOffer(stateDetails.playerSellOffers, offer);
				if (item.getStackAmount() <= 0)
				{
					stateDetails.playerOffers.remove(index);
				}
			}
			else
			{
				stateDetails.playerSellOffers.Add(offer);
				stateDetails.playerOffers.remove(index);
			}
			stateDetails.weight -= offer.item.getWeight();
			stateDetails.currentGold += offer.value;

			// int index = stateDetails.playerOffers.indexOf(offer);
			// Offer offer = stateDetails.playerOffers.get(index);
			// stateDetails.playerSellOffers.Add(offer);
			// stateDetails.playerOffers.remove(index);
			notifyObservers(stateDetails.playerOffers,
					PLAYER_INVENTORY_OFFERS_UPDATE);
			notifyObservers(stateDetails.playerSellOffers,
					PLAYER_SELL_OFFERS_UPDATE);
			// showOptionsForItem(offers.get(index).item,
			// ItemState.PLAYER_INVENTORY);
		}
		// REMOVE FROM SELL LIST
		else if (pane == TradePaneEnum.PLAYER_SELL)
		{
			int index = stateDetails.playerSellOffers.indexOf(offer);
			if (offer.item is StackableItem)
			{
				StackableItem item = (StackableItem) offer.item;
				OfferItemUtils.addOffer(stateDetails.playerOffers, offer);
				if (item.getStackAmount() <= 0)
				{
					stateDetails.playerSellOffers.remove(index);
				}
			}
			else
			{
				stateDetails.playerOffers.Add(offer);
				stateDetails.playerSellOffers.remove(index);
			}
			stateDetails.weight += offer.item.getWeight();
			stateDetails.currentGold -= offer.value;
			// showOptionsForItem(offers.get(index).item,
			// ItemState.PLAYER_SELL);
			// Offer offer = stateDetails.playerSellOffers.get(index);
			// stateDetails.playerOffers.Add(offer);
			// stateDetails.playerSellOffers.remove(index);
			// stateDetails.currentGold -= offer.value;
			notifyObservers(stateDetails.playerOffers,
					PLAYER_INVENTORY_OFFERS_UPDATE);
			notifyObservers(stateDetails.playerSellOffers,
					PLAYER_SELL_OFFERS_UPDATE);
		}

	}

	private void showOptionsForItem(Item item, ItemState state)
	{
		stateDetails.selectedItem = item;
		stateDetails.selectedItemState = state;
		List<ItemOption> options = new List<ItemOption>();
		switch (state)
		{
			case GROUND:
			{

				options.Add(ItemOption.THROW);
				options.Add(ItemOption.DESTROY);
				options.Add(ItemOption.TAKE);

				if (item is Consumable)
				{
					options.Add(ItemOption.USE);
				}
				break;
			}
			case INVENTORY:
			{
				options.Add(ItemOption.THROW);
				options.Add(ItemOption.DESTROY);
				options.Add(ItemOption.DROP);

				if (item is Consumable)
				{
					options.Add(ItemOption.USE);
				}
				break;
			}
			/*
			 * case NPC_INVENTORY: { options.Add(ItemOption.BUY);
			 * options.Add(ItemOption.TAKE); break; } case PLAYER_INVENTORY: {
			 * options.Add(ItemOption.BUY); options.Add(ItemOption.TAKE); break;
			 * } case NPC_SELL: { options.Add(ItemOption.BUY);
			 * options.Add(ItemOption.TAKE); break; } case PLAYER_SELL: {
			 * options.Add(ItemOption.BUY); options.Add(ItemOption.TAKE); break;
			 * }
			 */
			case NPC_INVENTORY:
			{

				break;
			}
			case PLAYER_INVENTORY:
			{
				options.Add(ItemOption.BUY);
				options.Add(ItemOption.TAKE);
				break;
			}
			case NPC_SELL:
			{
				options.Add(ItemOption.BUY);
				options.Add(ItemOption.TAKE);
				break;
			}
			case PLAYER_SELL:
			{
				options.Add(ItemOption.BUY);
				options.Add(ItemOption.TAKE);
				break;
			}
			default:
				break;

		}
		if (options.Count > 0)
		{
			stateDetails.selectedOptions = options;
			notifyObservers(options, ITEM_OPTIONS);
		}
	}

	// TODO
	public void itemOptionSelected(int storedIndex)
	{
		ItemOption option = stateDetails.selectedOptions.get(storedIndex);

		switch (option)
		{
			case BUY:
				if (stateDetails.selectedItemState == ItemState.NPC_INVENTORY)
				{

				}
				break;
			case DESTROY:
				if (stateDetails.selectedItemState == ItemState.INVENTORY)
				{
					stateDetails.items.remove(stateDetails.selectedItem);
					notifyObservers(stateDetails.items, SHOW_TILE_ITEMS_UPDATE);
				}
				else if (stateDetails.selectedItemState == ItemState.GROUND)
				{
					stateDetails.items.remove(stateDetails.selectedItem);
					notifyObservers(stateDetails.items, SHOW_TILE_ITEMS_UPDATE);
				}
				break;
			case DROP:
			{
				if (stateDetails.selectedItemState == ItemState.PLAYER_INVENTORY)
				{
					stateDetails.items.remove(stateDetails.selectedItem);
					getPlayerTile().addItem(stateDetails.selectedItem);
					notifyObservers(stateDetails.items, SHOW_TILE_ITEMS_UPDATE);
				}
				break;
			}
			case TAKE:
			{
				if (stateDetails.selectedItemState == ItemState.GROUND)
				{
					player.addItemToInventory(stateDetails.selectedItem);
					stateDetails.items.remove(stateDetails.selectedItem);
					notifyObservers(stateDetails.items, SHOW_TILE_ITEMS_UPDATE);
				}
				else if (stateDetails.selectedItemState == ItemState.NPC_INVENTORY)
				{

				}
				break;
			}
			case THROW:
				break;
			case USE:
			{
				// HMM?... TODO
				if (stateDetails.selectedItemState == ItemState.GROUND)
				{
					stateDetails.selectedItem.use(player);
					stateDetails.items.remove(stateDetails.selectedItem);
					notifyObservers(stateDetails.items, SHOW_TILE_ITEMS_UPDATE);
				}
				else if (stateDetails.selectedItemState == ItemState.PLAYER_INVENTORY)
				{
					stateDetails.selectedItem.use(player);
					stateDetails.items.remove(stateDetails.selectedItem);
					notifyObservers(stateDetails.items, SHOW_TILE_ITEMS_UPDATE);
				}
				break;
			}
			default:
				break;

		}

		notifyObservers("WAT", HIDE_OPTIONS);
	}

	public class ItemOption
	{
		public static readonly int THROW = 0;
		public static readonly int DROP = 1;
		public static readonly int USE = 2;
		public static readonly int DESTROY = 3;
		public static readonly int BUY = 4;
		public static readonly int TAKE = 5;
		
		//THROW("Throw"), DROP("Drop"), USE("Use"), DESTROY("Destroy"), BUY("Buy"), TAKE(
		//		"Take");

		
	 private static Dictionary <int, string> dict = new Dictionary<int, string> (){
	        { THROW, "Throw"},
	        { DROP, "Drop"},
	        { USE, "Use"},
	        { DESTROY, "Destroy"},
	        { BUY, "Buy"},
	        { TAKE, "Take"}//,
	       // { BOARD_TYPE, "BOARD_TYPE"}
	    };


		public string getName(int option)
		{
			return dict[option];
		}
	}

	enum ItemState {GROUND, INVENTORY, NPC_INVENTORY, NPC_SELL, PLAYER_INVENTORY, PLAYER_SELL};

	public void loot(int index)
	{
		Tile tile = getPlayerTile();
		List<Item> itens = tile.getItems(); // gib items plzz.
		if (itens.Count > index)
		{
			Item item = itens.get(index);
			player.addItemToInventory(item);
			itens.remove(index);
		}
	}

	public void drop(int index)
	{

	}

	/*
	 * public void respond(int index) { Tile tile = getPlayerTile(); Position
	 * position = tile.getPosition(); List<Position> npcPos =
	 * position.getNeighbours(); foreach (.* .* in npcPos) { Tile t =
	 * getTile(pos); if (t.hasUnit() && t.getUnit() is NPC) { NPC npc =
	 * (NPC) t.getUnit(); if (npc.hasResponses()) { List<string> replies =
	 * npc.getResponses(); if (replies.Count > index) { npc.reply(index); } } }
	 * } }
	 */
	public void advanceNPCDialog()
	{
		if (state == GameState.TALKING)
		{
			stateDetails.npc.advanceDialog();
			if (stateDetails.npc.isFinishedDialog())
			{
				// TODO
			}
		}
	}

	public void playerAttack()
	{
		Position origin = player.getPosition();
		Tile tile = getTile(origin);
		Direction direction = player.getFacing();

		int changeX = Direction.getChangeX(direction);
		int changeY = Direction.getChangeY(direction);

		List<Unit> damagedUnits = new List<Unit>();
		int atkRange = player.getWeaponAttackRange();

		bool didHit = false;
		for (int range = 1; range <= atkRange; range++)
		{
			if (!isOutOfBounds(origin, changeX * range, changeY * range))
			{
				Position newPosition = new Position(origin.row + changeY,
						origin.col + changeX);
				Tile targetTile = getTile(newPosition);
				if (targetTile.hasUnit())
				{
					Unit unit = targetTile.getUnit();
					int damageTaken = player.attack(unit);

					log("Player deals " + damageTaken + " to " + unit.getName());
					if (unit.isDead())
					{
						kill(player, targetTile);
						log(unit.getName() + " died.");
					}
					else
					{
						log(unit.getName() + " " + unit.getHealth() + "/"
								+ unit.getMaxHealth());
					}

					didHit = true;
					bool pierce = player.getWeaponPierce(unit);
					if (!pierce)
					{
						break;
					}
				}
			}
		}
		if (didHit)
		{
			player.getWeapon().degrade(1);
		}

		computerAction();
	}

	private void kill(Unit killer, Tile tile)
	{
		Unit ded = tile.getUnit();
		// Unit dead = targetTile.getUnit();
		gainEXP(killer, ded.getExpValue());
		tile.addItems(ded.getItems());
		if (ded.getGold() > 0)
		{
			tile.addItem(new Coins(ded.getGold()));
		}
		if (ded == player)
		{
			gameOver();
		}

		if (ded is NPC)
		{
			npcs.remove(ded);
		}
		if (ded is Enemy)
		{
			enemies.remove(ded);
		}
		tile.setUnit(null);
	}

	private void gainEXP(Unit unit, int expValue)
	{
		unit.addExp(expValue);
		if (unit.checkLEventelUp())
		{
			unit.lEventelUp();
		}
	}

	public void playerDirectionAction(Direction d)
	{
		if (d == Direction.NONE)
		{
			computerAction();
		}
		else
		{
			if (player.getFacing() != d)
			{
				player.facing = d;
			}
			else
			{
				int changeX = Direction.getChangeX(d);
				int changeY = Direction.getChangeY(d);

				Position origin = player.getPosition();

				if (!isOutOfBounds(origin, changeX, changeY))
				{
					Position newPosition = new Position(origin.row + changeY,
							origin.col + changeX);
					Tile tile = getTile(newPosition);
					if (canMoveToPosition(player, tile)
							&& !player.isOverweight())
					{
						move(player, newPosition);
						computerAction();
					}
					else
					{
						Debug.Log("CANNOT MOVE THERE or overweight.");
					}
				}

			}
		}
	}

	private void computerAction()
	{
		clearGameState();
		foreach (Enemy enemy in enemies)
		{
			enemy.makeMove();
		}
		npcAction();
	}

	private void npcAction()
	{
		foreach (NPC npc in npcs)
		{
			npc.makeMove();
		}

		extraActions();
	}

	private void extraActions()
	{
		player.workEOTStatusEffects();
		player.decayStatusEffects();

		nextDay();
	}

	private void nextDay()
	{
		day++;
	}

	private bool canMoveToPosition(Unit unit, Tile tile)
	{
		if (tile is River)
		{
			return unit.canSwim();
		}
		else if (tile is Wall)
		{
			return false;
		}
		else if (tile.hasUnit())
		{
			log("has unit");
			return false;
		}
		return true;
	}

	/**
	 * UTILITY
	 */
	public Tile getTile(Position pos)
	{
		if (pos.row < 0 || pos.row >= Constants.height || pos.col < 0
				|| pos.col >= Constants.width)
			return null;
		return board[pos.row][pos.col];
	}

	public Tile getPlayerTile()
	{
		return getTile(player.getPosition());
	}

	private bool isOutOfBounds(Position position, int changeX, int changeY)
	{
		int row = position.row;
		int col = position.col;
		if (row + changeY >= Constants.height || row + changeY < 0)
		{
			return true;
		}
		if (col + changeX >= Constants.width || col + changeX < 0)
		{
			return true;
		}
		return false;
	}

	private bool isOutOfBounds(Position position)
	{
		int row = position.row;
		int col = position.col;
		if (row >= Constants.height || row < 0)
		{
			return true;
		}
		if (col >= Constants.width || col < 0)
		{
			return true;
		}
		return false;
	}

	public Player getPlayer()
	{
		return player;
	}

	public List<Tile> getVisibleTiles(int vision, Position position)
	{

		List<Tile> tiles = new List<Tile>();
		for (int row = -vision; row <= position.row + vision; row++)
		{
			for (int col = -vision; col <= position.col + vision; col++)
			{
				tiles.Add(board[row][col]);
			}
		}
		return tiles;
	}

	public void unitRequestAttack(Unit atk, Tile tile)
	{
		log("UNIT ATTACKED!");
		// TODO
		// ALWAYS PERMIT FOR NOW.
		Unit def = tile.getUnit();
		if (def != null)
		{
			int damageTaken = atk.attack(def);

			log(atk.getName() + " attacks " + def.getName() + " for "
					+ damageTaken + ".");
			if (def.armor != null)
			{
				def.armor.degrade(1);
			}

			if (def.isDead())
			{
				kill(atk, tile);
			}

		}
	}

	public void unitRequestMoveTowardsPlayer(Unit unit, int velocity)
	{
		log("MOVED TOWARDS PLAYER " + velocity);
		List<Position> path = findPath(getTile(unit.getPosition()),
				getPlayerTile());
		if (path.Count - 1 < velocity)
		{
			velocity = path.Count - 1;
		}
		move(unit, path.get(velocity));
	}

	private void move(Unit unit, Position position)
	{

		// Debug.Log("MOVED TO TILE");
		Tile oldTile = getTile(unit.getPosition());
		oldTile.setUnit(null);
		unit.row = position.row;
		unit.col = position.col;
		Tile newTile = getTile(position);
		newTile.setUnit(unit);
	}

	private void move(Unit unit, Direction dir)
	{
		if (isOutOfBounds(unit.getPosition(), Direction.getChangeX(dir),
				Direction.getChangeY(dir)))
		{
			// Debug.Log("CANT MOVE -> WILL OUTTA BOUNDS");
			log("CANT MOVE -> WILL OUTTA BOUNDS");
		}
		else if (!canMoveToPosition(unit, getTile(unit.getPosition()
				.modifyByDirection(dir))))
		{
			log("UNIT CANNOT MOVE THAT DIRECTION");
		}
		else
		{
			move(unit, unit.getPosition().modifyByDirection(dir));
		}
	}

	private int max(int a, int b)
	{
		return a > b ? a : b;
	}

	private List<Position> findPath(Tile src, Tile des)
	{
		// List<Position> positions = new List<Position>();
		// positions.Add(src.getPosition());

		Unit unit = src.getUnit();
		///TODO
		/*PriorityQueue<Position> frontier = new PriorityQueue<Position>(10,
				new Comparator<Position>()
				{

					//Override
					public int compare(Position arg0, Position arg1)
					{
						int m1 = arg0.movesLeft;
						int m2 = arg1.movesLeft;
						if (m1 > m2)
						{
							return 1;
						}
						else if (m1 < m2)
						{
							return -1;
						}
						return 0;
					}
				});*/

		HashMap<Position, Integer> costMap = new HashMap<Position, Integer>();

		// List<Position> closed = new List<Position>();
		Position start = src.getPosition();
		start.parent = null;
		frontier.Add(start);
		costMap.put(start, 0);
		start.movesLeft = 0;
		Position goal = des.getPosition();
		Position current = null;
		while (!frontier.isEmpty())
		{
			current = frontier.remove();
			if (current.equals(goal))
			{
				break;
			}
			foreach (Position pos in current.getNeighbours())
			{
				int cost = 0;
				if (getTile(next).getUnit() != null
						|| !canMoveToPosition(unit, getTile(next)))
				{
					cost = 999;
				}
				else
				{
					cost = 1;
				}
				int new_cost = cost + costMap.get(current);
				Integer prEventCost = costMap.get(next);
				if (prEventCost == null || new_cost < prEventCost)
				{
					costMap.put(next, new_cost);
					next.movesLeft = new_cost + heuristic(next, goal);
					frontier.Add(next);
					next.parent = current;
				}
			}
			// closed.Add(current);
		}
		List<Position> path = new List<Position>();
		path.Add(current);
		while (!current.equals(start))
		{
			current = current.parent;
			path.Add(current);
		}
		Collections.rEventerse(path);

		return path;
	}

	private int heuristic(Position src, Position des)
	{
		return Math.abs(src.row - des.row) + Math.abs(src.col - des.col);
	}

	public void unitRequestMove(Unit unit, int velocity)
	{
		log("RANDOM MOVE");
		for (int i = 0; i < velocity; i++)
		{
			// move(unit, position);
			// Direction direct = random
			move(unit, Direction.randomDirection());
		}

	}

	// TODO
	public void playerPrimaryAction()
	{
		log("Primary");
		Position pos = player.getPosition().modifyByDirection(
				player.getFacing());
		if (!isOutOfBounds(pos))
		{
			Tile target = getTile(pos);
			if (target.hasUnit() && target.getUnit() is NPC)
			{
				beginConversation((NPC) target.getUnit());
			}
			else
			{
				playerAttack();
			}
		}
	}

	// TODO
	public void playerSecondaryAction()
	{
		setGameState(GameState.EXAMINE_GROUND);
		stateDetails.items = getPlayerTile().getItems();
		notifyObservers(stateDetails.items, SHOW_TILE_ITEMS);

		log("Secondary");
	}

	private void beginConversation(NPC npc)
	{
		setGameState(GameState.TALKING);
		stateDetails.npc = npc;
		notifyObservers(npc.getCurrentDialogMessage(), SHOW_TEXT);
		if (npc.canTrade())
		{
			MerchantNPC tradeNPC = (MerchantNPC) npc;
			beginTrade(tradeNPC);
		}
		List<string> options = npc.getCurrentOptions();
		if (options != null)
		{
			notifyObservers(options, NPC_OPTIONS);
		}

	}

	private void beginTrade(MerchantNPC npc)
	{
		log("NPC HAS " + npc.getItems().Count);
		List<Offer> npcOffers = npc.getOffers(player.getCharm());
		List<Offer> playerOffers = npc.getOfferForItems(player.getItems(),
				player.getCharm());
		stateDetails.maxWeight = player.getMaxWeight();
		stateDetails.weight = player.getCurrentWeight();
		stateDetails.originalGold = player.getGold();
		stateDetails.currentGold = stateDetails.originalGold;
		stateDetails.playerOffers = playerOffers;
		stateDetails.npcOffers = npcOffers;
		stateDetails.npcSellOffers = new List<Offer>();
		stateDetails.playerSellOffers = new List<Offer>();

		notifyObservers(npcOffers, NPC_INVENTORY_OFFERS);
		notifyObservers(playerOffers, PLAYER_INVENTORY_OFFERS);
		notifyObservers(stateDetails.npcSellOffers, NPC_SELL_OFFERS);
		notifyObservers(stateDetails.playerSellOffers, PLAYER_SELL_OFFERS);
	}

	public void confirmTrade(bool didConfirm)
	{
		if (didConfirm)
		{
			MerchantNPC npc = (MerchantNPC) stateDetails.npc;
			if (stateDetails.currentGold >= 0)
			{

				stateDetails.executeTrade();

				computerAction();
				// npc set gold?
			}
			else
			{
				notifyObservers("CANNOT TRADE. NOT ENOUGH MONEY",
						SHOW_ERROR_DIALOG);
				log("CANNOT HAVE NEGATIVE MONEY");
			}
		}
		else
		{
			clearGameState();
		}

	}
	
	public void npcOptionSelected(int storedIndex)
	{
		stateDetails.npc.optionSelected(storedIndex);

		notifyObservers("WAT", HIDE_OPTIONS);
	}

	public void cancelTrade()
	{
		/*
		 * MerchantNPC npc = (MerchantNPC) stateDetails.npc; if
		 * (stateDetails.currentGold >= 0) {
		 * player.setGold(stateDetails.currentGold);
		 * player.addBoughtOffers(stateDetails.buyOffers);
		 * npc.addBoughtOffers(stateDetails.sellOffers); computerAction(); //npc
		 * set gold? } else { log("CANNOT HAVE NEGATIVE MONEY"); }
		 */
		computerAction();
	}

	enum GameState
	{
		TALKING, FREE, EXAMINE_INVENTORY, EXAMINE_GROUND
	};

	public class GameStateDetails
	{
		public int weight;
		public int maxWeight;
		public int currentGold;
		public List<ItemOption> selectedOptions;
		public ItemState selectedItemState;
		public Item selectedItem;
		NPC npc;

		List<Offer> playerSellOffers;
		List<Offer> npcSellOffers;
		List<Offer> playerOffers;
		List<Offer> npcOffers;
		List<Item> items;
		int originalGold;

		public string generatePlayerDetails()
		{
			return string.format("Weight %d/%d Gold %d", weight, maxWeight,
					currentGold);
		}

		public void executeTrade()
		{
			player.setGold(stateDetails.currentGold);

			player.addBoughtOffers(stateDetails.npcSellOffers);
			((MerchantNPC) npc).addBoughtOffers(stateDetails.playerSellOffers);

			player.removeSoldOffers(stateDetails.playerSellOffers);
			((MerchantNPC) npc).removeSoldOffers(stateDetails.playerSellOffers);

			npc = null;
			stateDetails.playerOffers = null;
			stateDetails.playerSellOffers = null;
			stateDetails.npcOffers = null;
			stateDetails.npcSellOffers = null;

			setGameState(GameState.FREE);
		}

		public void reset()
		{

			if (npcSellOffers != null)
			{
				foreach (Offer offer in npcSellOffers)
				{
					npc.addItemToInventory(offer.item);
					// OfferItemUtils.addItem(npc.items, offer.item);
				}
			}
			if (playerSellOffers != null)
			{
				foreach (Offer offer in playerSellOffers)
					player.addItemToInventory(offer.item);
				// OfferItemUtils.addItem(player.getItems(), offer.item);
			}

			if (npc != null)
			{
				npc.resetDialog();
				npc = null;
			}
			currentGold = 0;
			originalGold = 0;
			weight = 0;
			maxWeight = 0;

			npcSellOffers = null;
			playerSellOffers = null;
			playerOffers = null;
			npcOffers = null;
			items = null;

		}
	}

	private void setGameState(GameState state)
	{
		this.state = state;
		notifyObservers(state, GAME_STATE_UPDATE);
	}

	private void clearGameState()
	{
		setGameState(GameState.FREE);
		stateDetails.reset();
	}

	public void subscribe(BoardObs obs)
	{
		observers.Add(obs);
	}

	private void notifyObservers(Object object, string reason)
	{
		foreach (BoardObs observer in observers)
		{
			observer.updateUI(object, reason);
		}
	}

	public GameState getState()
	{
		return state;
	}

	public void requestIntegerValue(Requester source, string message, int gold)
	{
		ObjectRequest obj = new ObjectRequest();
		obj.source = source;
		obj.message = message;
		obj.obj = gold;
		notifyObservers(obj, REQUEST_INTEGER_VALUE);
	}

	public void satisfyIntegerRequest(ObjectRequest obj)
	{
		if (obj.cancelled)
		{
			obj.source.satisfyIntegerValue(0);
		}
		else
		{
			obj.source.satisfyIntegerValue((Integer) obj.ret);
		}
	}

	public static void log(string s)
	{
		if (debug)
			Debug.Log(s);
	}

}