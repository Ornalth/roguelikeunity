







public class OfferItemUtils
{
	public static void addOffer(List<Offer> offers, Offer newOffer)
	{
		if (newOffer.item is StackableItem)
		{
			bool success = false;
			
			foreach (Offer offer in offers)
			{
				//Found just add one to stack
				if (offer.item is StackableItem && ((StackableItem)offer.item).canJoin((StackableItem)newOffer.item))
				{
					((StackableItem)offer.item).addStackAmount(1);
					
					success = true;
					break;
				}
			}
			if (!success)
			{
				Offer offer = new Offer();
				offer.item = ((StackableItem)newOffer.item).deepCopy();
				((StackableItem)offer.item).setStackAmount(1);
				offers.Add(offer);
				offer.value = newOffer.value;
			}
			
			((StackableItem)newOffer.item).removeStackAmount(1);
			//if(((StackableItem)newOffer.item).getStackAmount() == 0)
			//{
				//newOffer.value = 0;
				//newOffer = null;
			//}
		}
		else
		{
			offers.Add(newOffer);
		}
	}
	
	public static void addItem(List<Item> items, Item newItem)
	{
		if (newItem is StackableItem)
		{
			bool success = false;
	
			foreach (Item item in items)
			{
				if (item is StackableItem && ((StackableItem)item).join((StackableItem)newItem))
				{
					success = true;
					break;
				}
			}
			if (!success)
			{
				items.Add(newItem);
			}
		}
		else
		{
			items.Add(newItem);
		}
	}
}
