

public interface Attackable
{
	void takeDamage();
	void attack();
	void isDead();
}
