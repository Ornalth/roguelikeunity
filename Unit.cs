














public abstract class Unit
{
	public int row;
	public int col;

	
	protected int level;
	protected int maxHP;
	protected int hp;
	protected double mp;
	protected double maxMP;
	protected int currentExp;
	protected int gold;
	
	protected int def;
	
	protected double str;
	protected double intellect;
	protected double charm;
	protected double agi;
	//protected int speed;
	protected int vision;

	protected UnitModifier modifier = UnitModifier.NONE;
	public FaithAlignment align = new FaithAlignment();
	readonly protected List<Item> items = new List<Item>();
	protected List<StatusEffect> statuses = new List<StatusEffect>();
	
	protected Weapon weapon;
	protected Armor armor;
	protected Accessory accessory;
	
	protected Direction facing = Direction.SOUTH;
	
	public int getLevel()
	{
		return level;
	}
	public void setLevel(int level)
	{
		this.level = level;
	}
	public int getMaxHP()
	{
		return maxHP;
	}
	public void setMaxHP(int maxHP)
	{
		this.maxHP = maxHP;
		this.hp = maxHP;
	}
	public int getHp()
	{
		return hp;
	}
	public void setHp(int hp)
	{
		this.hp = hp;
	}
	public double getMp()
	{
		return mp;
	}
	public void setMp(double mp)
	{
		this.mp = mp;
	}
	public double getMaxMP()
	{
		return maxMP;
	}
	public void setMaxMP(double maxMP)
	{
		this.mp = maxMP;
		this.maxMP = maxMP;
	}

	public int getDef()
	{
		return def;
	}
	public void setDef(int def)
	{
		this.def = def;
	}
	public int getGold()
	{
		return gold;
	}
	public void setGold(int gold)
	{
		this.gold = gold;
	}
//	public int getSpeed()
//	{
//		return speed;
//	}
//	public void setSpeed(int speed)
//	{
//		this.speed = speed;
//	}
	

	public int getVision()
	{
		return vision + modifier.getSightModifier();
	}
	public int getStr()
	{
		return (int)str;
	}
	public void setStr(double str)
	{
		this.str = str;
	}
	public int getIntellect()
	{
		return (int)intellect;
	}
	public void setIntellect(double intellect)
	{
		this.intellect = intellect;
	}
	public int getCharm()
	{
		return (int)charm;
	}
	public void setCharm(double charm)
	{
		this.charm = charm;
	}
	public int  getAgi()
	{
		return (int)agi;
	}
	public void setAgi(double agi)
	{
		this.agi = agi;
	}
	public void setVision(int vision)
	{
		this.vision = vision;
	}
	public int getCurrentExp()
	{
		return currentExp;
	}
	public void setCurrentExp(int currentExp)
	{
		this.currentExp = currentExp;
	}
	public void setWeapon(Weapon weapon)
	{
		this.weapon = weapon;
	}
	public void setArmor(Armor armor)
	{
		this.armor = armor;
	}
	public void setAccessory(Accessory accessory)
	{
		this.accessory = accessory;
	}

	//int discipline;
	
	abstract public int takeDamage (Unit source, int baseDamage);
	abstract public int takeDamage (EVENT ev, int baseDamage);
	
	private void takeDamage(StatusEffect effect, int dmg)
	{
		this.hp -= dmg;
		Board.log(getName() + " takes " + dmg + " from " + effect.status.getName());
	}
	

	public int estimateDamage (Unit unit)
	{
		int attack = getWeaponAttack();
		if (attack == 0)
		{
			return (int) (getStr() * getDamageModifiers());
		}
		else
		{
			int dmg = (int) (attack * str / 5 * getDamageModifiers());
			return dmg;
		}
		//return  + weapon.getAttack(unit);
	}
	
	private double getDamageModifiers()
	{
		double mod = 1;
		if (modifier != null)
			mod *= modifier.getAttackModifier();
		if (this.weapon != null)
			mod *= weapon.getModifier().getAttackModifier();
		if (this.accessory != null)
			mod *= accessory.getModifier().getAttackModifier();
		if (this.armor != null)
			mod *= armor.getModifier().getAttackModifier();
		
		return mod;
	}
	
	public bool isDead()
	{
		return hp <= 0;
	}
	
	public Position getPosition()
	{
		return new Position(row,col);
	}
	
	public bool canSwim()
	{
		return false;
	}
	abstract public string getDrawableName();

	public Direction getFacing()
	{
		return facing;
	}
	
	public void setFacing(Direction d)
	{
		facing = d;
	}
	//public void attack ()
	public void addGold(int value)
	{
		gold += value;
	}
	
	public List<Item> getItems()
	{
		return items;
	}
	

	public Weapon getWeapon()
	{
		return weapon;
	}
	
	public Armor getArmor()
	{
		return armor;
	}
	
	public Accessory getAccessory()
	{
		return accessory;
	}
	
	abstract public string getName();
	
	abstract public int getExpValue();
	public void addExp(int expValue)
	{
		currentExp += expValue;
	}
	
	public bool checkLevelUp()
	{
		if (currentExp >= getNextLevelExp())
		{
			return true;
		}
		return false;
	}
	
	abstract public int getNextLevelExp();
	abstract protected void gainStats();
	
	//Override for special effects?.
	public int attack(Unit unit)
	{
		int estimate = this.estimateDamage(unit);
		return unit.takeDamage(this, estimate);
	}
	
	public void levelUp()
	{
		if (checkLevelUp())
		{
			currentExp -= getNextLevelExp();
			level += 1;
			gainStats();
			levelUp();
		}
	}

	public int getHealth()
	{
		return hp;
	}
	
	public int getMaxHealth()
	{
		return maxHP;
	}

	public UnitModifier getModifier()
	{
		return modifier;
	}
	public void setModifier(UnitModifier modifier)
	{
		this.modifier = modifier;
	}
	
	protected void addItemToInventory(Item newItem)
	{
		OfferItemUtils.addItem(this.items, newItem);
	}
	
	public List<Item> getInventory()
	{
		return items;
	}
	
	public void decayStatusEffects()
	{
		List<StatusEffect> remove = new List<StatusEffect>();
		foreach (StatusEffect effect in statuses)
		{
			effect.decay();
			if (effect.isFinished())
			{
				remove.Add(effect);
			}
		}
		statuses.removeAll(remove);
	}
	
	public void workEOTStatusEffects()
	{
		foreach (StatusEffect effect in statuses)
		{
			switch (effect.status)
			{
				case BURNING:
				{
					this.takeDamage(effect, effect.lvl);
					break;
				}
				default:
					break;
				
			}
		}
	}

	public void addStatusEffect(StatusEffect effect)
	{
		int index = Utils.getStatusEffectIndex(statuses, effect);
		if (index != -1)
		{
			StatusEffect curEffect = statuses.get(index);
			if (curEffect.lvl < effect.lvl)
			{
				curEffect.lvl = effect.lvl;
				curEffect.duration = effect.duration;
			}
			else if (curEffect.lvl == effect.lvl)
			{
				curEffect.duration = Math.max(curEffect.duration, effect.duration);
			}
		}
		else
		{
			statuses.Add(effect);
		}
		
		Board.log(getName() + " has received " + effect.status.getName());
	}

	public int getWeaponAttack()
	{
		if (weapon!= null)
		{
			return weapon.getAttack();
		}
		return 0;
	}
	

}
