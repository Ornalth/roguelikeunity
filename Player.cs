

// LEGEND @ - enemies, E - Incidents, I- items, P - player, B - bosses


















public class Player : Unit
{
	Random rand = new Random();

	int exp = 0;
	int nextLvl = 10;
	
	int inventoryMax = 10;
	int bonus = 0;
	int maxWeight;
	// int discipline;
	double lastCost;
	

	//List<Item> items = new List<Item>(inventoryMax);
	List<StatusEffect> effects = new List<StatusEffect>(10);
	List<Skill> skills = new List<Skill>();
	List<KeyItem> keyItems = new List<KeyItem>();
	Player()
	{
		// wep = new Equipment("startWep");
		// arm = new Equipment("startArm");
		level = 1;
		hp = 10;
		maxHP = 10;
		mp = 0.0;
		maxMP = 3.0;
		def = 0;
		exp = 0;
		facing = Direction.SOUTH;
		weapon = new Pickaxe(100);
		armor = new SlimeArmor(100);
		accessory = new WorkGlove(100);
		items.Add(weapon);
		items.Add(armor);
		items.Add(accessory);
		// discipline = 0;
		maxWeight = 10;
		
		str =5;
		charm = 1;
		agi = 1;
		intellect = 1;
		//agi = 5;
		
		items.Add(ItemFactory.createConsumable());
	}

	public void setRow(int rol)
	{
		row = rol;
	}

	public void setCol(int col)
	{
		this.col = col;
	}


	//TODO armor
	//Override
	public int takeDamage(Unit source, int baseDamage)
	{
		int damage = baseDamage - def - armor.getDefense(source);
		if (damage >= 0)
		{
			hp -= damage;
			return damage;
		}
		return 0;
		
	}

	//Override
	public int takeDamage(EVENT ev, int baseDamage)
	{
		hp -= baseDamage;
		return baseDamage;
	}
	
	public void move(Position newPosition)
	{
		row = newPosition.row;
		col = newPosition.col;
	}

	//TODO
	public bool hasKeyItem(string str)
	{
		/*foreach (KeyItem item in keyItems)
		{
			
		}*/
		return false;
	}

	public int getCurrentWeight()
	{
		int weight = 0;
		foreach (Item item in items)
		{
			if (item is StackableItem)
			{
				weight += item.getWeight() * ((StackableItem)item).getStackAmount();
			}
			else
				weight += item.getWeight();
		}
		return weight;
	}
	
	//Override
	public bool canSwim()
	{
		if(hasKeyItem("FLIPPERS"))
		{
			return true;
		}
		return false;
	}

	//Override
	public string getDrawableName()
	{
		return "player";
	}

	public bool isOverweight()
	{
		return getCurrentWeight() > maxWeight;
	}
	
	public void equip(Equipment equip)
	{
		//List<Item> items = getItems();
		//int index = items.indexOf(equip);
		//items.remove(index);
		
		if (equip is Weapon)
		{
			//if (weapon != null)
			//{
			//	items.Add(weapon);	
			//}
			weapon = (Weapon) equip;
		}
		else if (equip is Armor)
		{
			//if (armor != null)
			//{
			//	items.Add(armor);	
			//}
			armor = (Armor) equip;
		}
		else if (equip is Accessory)
		{
			//if (accessory != null)
			//{
			//	items.Add(accessory);	
			//}
			accessory = (Accessory) equip;
		}
	}

	public void heal(int value)
	{
		hp += value;
		if (hp > maxHP)
			hp = maxHP;
		//hp = Utils.max(hp, maxHP);
	}

	public int getWeaponAttackRange()
	{
		if (weapon == null)
		{
			return 1;
		}
		return weapon.getRange();
	}

	public bool getWeaponPierce(Unit unit)
	{
		if (weapon == null)
		{
			return false;
		}
		return weapon.shouldPierce(unit);
	}

	//Override
	public int getExpValue()
	{
		return 0;
	}

	//Override
	public int getNextLevelExp()
	{
		return level * level * 5;
	}

	//Override
	protected void gainStats()
	{
		str += 0.5;
		charm += 0.5;
		agi += 0.5;
		intellect += 0.5;
		
		def += 1;
		
		maxHP += 5;
		hp += 5;
		maxMP += 1;
		mp += 1;
	}

	//Override
	public int getLevel()
	{
		return level;
	}
	
	public int getMaxWeight()
	{
		return maxWeight;
	}
	
	//Override
	public int getGold()
	{
		return gold;
	}
	
	//Override
	public string getName()
	{
		return "PLAYER";
	}

	public void addBoughtOffers(List<Offer> offers)
	{
		foreach (Offer offer in offers)
		{
			addItemToInventory(offer.item);
		}
	}
	
	public void removeSoldOffers(List<Offer> offers)
	{
		foreach (Offer offer in offers)
		{
			
			if (offer.item is StackableItem)
			{
				List<Item> remove = new List<Item>();
				foreach (Item item in items)
				{
					if (item is StackableItem)
					{
						StackableItem stack = (StackableItem) item;
						if (stack.canJoin((StackableItem) offer.item))
						{
							int normalStackCount = stack.getStackAmount();
							int sellStackCount = ((StackableItem)offer.item).getStackAmount();
							if (normalStackCount == 0)
							{
								remove.Add(item);
							}
							break;
						}
					}
				}
				items.removeAll(remove);
			
			}
			else
			{
				items.remove(offer.item);
			}
			
			if (weapon == offer.item)
			{
				weapon = null;
			}
			else if (armor == offer.item)
			{
				armor = null;
			}
			else if (accessory == offer.item)
			{
				accessory = null;
			}
		}
	}

	public void healMP(int value)
	{
		mp += value;
		if (mp > maxMP)
			mp = maxMP;
	}


}
