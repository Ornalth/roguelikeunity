

public interface BoardObs
{
	void updateUI(Object object, string reason);
}
