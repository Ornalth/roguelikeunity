

public class Modifier
{
	public double attackModifier;
	public double defenseModifier;
	public double AgiModifier;
	public double intModifier;
	public double charmModifier;
	public int sightModifier;
	public double expModifier;
	// int rangeModifier;
	public Modifier(double attackModifier, double defenseModifier,
			double AgiModifier, double intModifier, double charmModifier,
			int sightModifier, double expModifier)
	{
		this.attackModifier = attackModifier;
		this.defenseModifier = defenseModifier;
		this.AgiModifier = AgiModifier;
		this.intModifier = intModifier;
		this.charmModifier = charmModifier;
		this.sightModifier = sightModifier;
		this.expModifier = expModifier;
	}
}
