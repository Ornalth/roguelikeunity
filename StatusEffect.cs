

public class StatusEffect
{
	private static int FOREVER = 9999;
	public Status status;
	public int duration;
	public int lvl;
	public bool doesDecay = true;
	
	public StatusEffect(Status s, int l, int d)
	{
		status = s;
		lvl = l;
		duration = d;
	}
	public StatusEffect(Status s, int level)
	{
		this(s,level,FOREVER);
	}
	public StatusEffect(Status s)
	{
		this(s,1,FOREVER);
	}


	public void decay()
	{
		if (duration != FOREVER && doesDecay)
		{
			duration--;
		}
	}
	public bool isFinished()
	{
		if (duration <= 0)
			return true;
		return false;
	}
	
	

	public class Status
	{
		public static readonly int BURNING = 1;

		
	 private static Dictionary <int, string> dict = new Dictionary<int, string> (){
	        { BURNING, "BURNING"}
	       // { BOARD_TYPE, "BOARD_TYPE"}
	    };

		
		public string getName(int val)
		{
			return dict[val];
		}
	}
}
