





public abstract class Armor : Equipment
{

	protected int defense;
	abstract public int getDefense(Unit unit); // Vs unit
	
	Armor(int durability)
	{
		super(durability);
	}
	
	//Override
	public bool shouldDisappear()
	{
		// TODO Auto-generated method stub
		return false;
	}
	

	//Override
	public string getImageDrawable()
	{
		return "armor";
	}
}
