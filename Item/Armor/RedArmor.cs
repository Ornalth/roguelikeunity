





public class RedArmor : Armor
{
	
	public RedArmor(int durability)
	{
		super(durability);
		defense = 5;
	}


	//Override
	public int getDefense(Unit unit)
	{
		return defense;
	}


	//Override
	public string getStoreText()
	{
		return "Red Armor";
	}


	//Override
	public int getBaseGoldValue()
	{
		return 500;
	}

	//Override
	public int getWeight()
	{
		return 5;
	}
}
