



public class YellowArmor : Armor
{
	
	public YellowArmor(int durability)
	{
		super(durability);
		defense = 3;
	}


	//Override
	public int getDefense(Unit unit)
	{
		return defense;
	}


	//Override
	public string getStoreText()
	{
		return "Yellow Armor";
	}


	//Override
	public int getBaseGoldValue()
	{
		return 150;
	}

	//Override
	public int getWeight()
	{
		return 3;
	}
}
