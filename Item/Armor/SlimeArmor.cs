



public class SlimeArmor : Armor
{
	
	public SlimeArmor(int durability)
	{
		super(durability);
		defense = 1;
	}


	//Override
	public int getDefense(Unit unit)
	{
		return defense;
	}


	//Override
	public string getStoreText()
	{
		return "Slime Armor";
	}


	//Override
	public int getBaseGoldValue()
	{
		return 250;
	}

	//Override
	public int getWeight()
	{
		return 1;
	}
}
