












public class EquipmentFactory
{
	private static Random rand = new Random();

	public static Weapon createWeapon()
	{
		Weapon weapon = null;
		switch(rand.nextInt(3))
		{
			case 0:
				weapon = new Axe(100 + rand.nextInt(24));
				break;
			case 1:
				weapon = new Dagger(100 + rand.nextInt(24));
			case 2:
				weapon = new Pickaxe(100 + rand.nextInt(24));
				
		}
		weapon.setModifier(EquipmentModifier.getRandomModifier());
		return weapon;
	}

	public static Armor createArmor()
	{
		Armor armor = null;
		switch(rand.nextInt(3))
		{
			case 0:
				armor = new RedArmor(100 + rand.nextInt(24));
				break;
			case 1:
				armor = new SlimeArmor(100 + rand.nextInt(24));
			case 2:
				armor = new YellowArmor(100 + rand.nextInt(24));
				
		}
		armor.setModifier(EquipmentModifier.getRandomModifier());
		return armor;
	}
}
