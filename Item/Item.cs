
// LEGEND @ - enemies, E - Inctypeents, I- items, P - player, B - bosses




public abstract class Item : NPCStorestringValue{
	
	string name;
	string desc;
	//protected int weight;
	Random rand = new Random();
	
	public string getName()
	{
		return name;
	}
	abstract public int getWeight();
	//{
		//return weight;
	//}
	
	abstract public bool shouldDisappear();
	abstract public void use(Player origin);
	abstract public int getBaseGoldValue();
	
	//Override 
	public string tostring()
	{
		return getStoreText();
	}

	abstract public string getImageDrawable();

}
