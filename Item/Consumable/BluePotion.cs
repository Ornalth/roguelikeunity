






public class BluePotion : Consumable
{


	int mpGain = 3;
	
	public BluePotion()
	{
		super();
		this.stack = 1;	
		 mpGain = 3;
		
	}
	
	//Override
	public void use(Player origin)
	{
		origin.healMP(mpGain);
		stack--;
	}

	//Override
	public bool join(StackableItem item)
	{
		if (item is BluePotion)
		{
			this.stack += item.getStackAmount();
			return true;
		}
		return false;
	}

	//Override
	public string getStoreText()
	{
		return "MP potion";
	}



	//Override
	public string getImageDrawable()
	{
		return "redPotion";
	}
	
	//Override
	public int getBaseGoldValue()
	{
		return 75;
	}

	//Override
	public bool canJoin(StackableItem item)
	{
		if (item is BluePotion)
		{
			return true;
		}
		return false;
	}
	

	//Override
	public Item deepCopy()
	{
		BluePotion potion = new BluePotion();
		potion.stack = this.stack;
		return potion;
	}

	//Override
	public int getWeight()
	{
		return 1;
	}
}
