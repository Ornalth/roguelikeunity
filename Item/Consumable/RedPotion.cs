






public class RedPotion : Consumable
{


	int hpGain = 100;

	public RedPotion()
	{
		super();
		this.stack = 1;	
		hpGain = 100;
		
	}
	
	//Override
	public void use(Player origin)
	{
		origin.heal(hpGain);
		stack--;
	}

	//Override
	public bool join(StackableItem item)
	{
		if (item is RedPotion)
		{
			this.stack += item.getStackAmount();
			return true;
		}
		return false;
	}

	//Override
	public string getStoreText()
	{
		return "Health potion";
	}



	//Override
	public string getImageDrawable()
	{
		return "redPotion";
	}
	
	//Override
	public int getBaseGoldValue()
	{
		return 50;
	}

	//Override
	public bool canJoin(StackableItem item)
	{
		if (item is RedPotion)
		{
			return true;
		}
		return false;
	}
	

	//Override
	public Item deepCopy()
	{
		RedPotion potion = new RedPotion();
		potion.stack = this.stack;
		return potion;
	}

	//Override
	public int getWeight()
	{
		return 1;
	}
}
