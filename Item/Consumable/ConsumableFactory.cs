





public class ConsumableFactory
{
	private static Random rand = new Random();
	
	public static Item createConsumable()
	{
		switch (rand.nextInt(3))
		{
			case 0:
				return new RedPotion();
			case 1:
				return new BluePotion();
			case 2:
				return new PurplePotion();
		}
		return new RedPotion();
	}
}
