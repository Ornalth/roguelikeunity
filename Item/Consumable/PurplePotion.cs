






public class PurplePotion : Consumable
{


	int hpGain = 80;
	int mpGain = 1;
	
	public PurplePotion()
	{
		super();
		this.stack = 1;	
		
	}
	
	//Override
	public void use(Player origin)
	{
		origin.heal(hpGain);
		origin.healMP(mpGain);
		stack--;
	}

	//Override
	public bool join(StackableItem item)
	{
		if (item is PurplePotion)
		{
			this.stack += item.getStackAmount();
			return true;
		}
		return false;
	}

	//Override
	public string getStoreText()
	{
		return "Purple potion";
	}



	//Override
	public string getImageDrawable()
	{
		return "redPotion";
	}
	
	//Override
	public int getBaseGoldValue()
	{
		return 100;
	}

	//Override
	public bool canJoin(StackableItem item)
	{
		if (item is PurplePotion)
		{
			return true;
		}
		return false;
	}
	

	//Override
	public Item deepCopy()
	{
		PurplePotion potion = new PurplePotion();
		potion.stack = this.stack;
		return potion;
	}

	//Override
	public int getWeight()
	{
		return 1;
	}
}
