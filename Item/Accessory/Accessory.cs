




public abstract class Accessory : Equipment
{
	protected Accessory(int durability)
	{
		super(durability);
	}

	abstract public int getAttackModifier(Unit unit);
	
}
