




public class WorkGlove : Accessory
{
	public WorkGlove(int durability)
	{
		super(durability);
		// TODO Auto-generated static readonlyructor stub
	}
	int bonusDamagePercent = 5;
	//Override
	public int getAttackModifier(Unit unit)
	{
		return bonusDamagePercent;
	}
	//Override
	public string getStoreText()
	{
		return "Work Glove";
	}

	//Override
	public string getImageDrawable()
	{
		return "workglove";
	}

	//Override
	public int getBaseGoldValue()
	{
		return 80;
	}
	

	//Override
	public int getWeight()
	{
		return 2;
	}
}
