




public class WoodenArrow : Accessory
{

	public WoodenArrow(int durability)
	{
		super(durability);
	}

	//Override
	public int getAttackModifier(Unit unit)
	{
		return 1;
	}

	//Override
	public string getStoreText()
	{
		return "Wooden Arrows";
	}

	
	
	//Override
	public string getImageDrawable()
	{
		return "woodArrow";
	}
	
	//Override
	public int getBaseGoldValue()
	{
		return 5;
	}


	//Override
	public int getWeight()
	{
		return 0;
	}

}
