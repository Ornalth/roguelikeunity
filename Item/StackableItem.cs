

public abstract class StackableItem : Item
{
	protected int stack = 0;
	abstract public bool canJoin(StackableItem item);
	abstract public bool join(StackableItem item);
	
	public int getStackAmount()
	{
		return stack;
	}
	
	public void addStackAmount(int i)
	{
		int amt = getStackAmount();
		amt += i;
		setStackAmount(amt);
	}
	
	public void setStackAmount(int amt)
	{
		stack = amt;		
	}
	
	public void removeStackAmount(int amt)
	{
		stack -= amt;
	}
	
	abstract public Item deepCopy();
}
