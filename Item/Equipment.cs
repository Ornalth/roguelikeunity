





public abstract class Equipment : Item
{
	int durability;
	protected EquipmentModifier modifier = EquipmentModifier.NONE;
	//TODO effects later.
	
	protected Equipment (int durability)
	{
		
	}
	//Override
	public void use(Player origin)
	{
		origin.equip(this);
	}
	
	//Override
	public bool shouldDisappear()
	{
		if (durability <= 0)
		{
			return true;
		}
		return false;
	}
	
	public void degrade(int amount)
	{
		durability -= amount;
	}
	
	public EquipmentModifier getModifier()
	{
		return modifier;
	}
	
	public void setModifier(EquipmentModifier mod)
	{
		this.modifier = mod;
	}
	
}
