














public class ItemFactory
{
	private static int numItems = 10;
	private static Random rand = new Random();

	public static Item createItem()
	{
		while (true)
		{
			switch (rand.nextInt(numItems))
			{
				case 0:
					return new Coins();
				case 1:
					return new WoodenArrow(rand.nextInt(30) + 5);
				case 2:
					return new WorkGlove(rand.nextInt(100) + 24);
				case 3:
					return new SlimeArmor(rand.nextInt(100) + 24);
				case 4:
					return new RedPotion();
				case 5:
					return new Pickaxe(rand.nextInt(100) + 24);
				case 6:
					return new Axe(rand.nextInt(100) + 24);
				case 7:
					return new Dagger(rand.nextInt(100) + 24);
				case 8:
					return new YellowArmor(rand.nextInt(100) + 24);
				case 9:
					return new RedArmor(rand.nextInt(100) + 24);
			}
		}

	}

	public static Item createConsumable()
	{
		return ConsumableFactory.createConsumable();
		
	}

	public static Item createWeapon()
	{
		return EquipmentFactory.createWeapon();
	}
	
	public static Item createArmor()
	{
		// TODO Auto-generated method stub
		return EquipmentFactory.createArmor();
	}


}
