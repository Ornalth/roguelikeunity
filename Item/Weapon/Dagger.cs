



public class Dagger : Weapon
{
	
	public Dagger(int durability)
	{
		super(durability);
		attack = 5;
		speed = 3;
		range = 1;
	}

	//Override
	public int getAttack(Unit unit)
	{

		return attack;
	}

	//Override
	public int getSpeed()
	{
		return speed;
	}

	//Override
	public bool shouldPierce(Unit unit)
	{
		return false;
	}

	//Override
	public string getStoreText()
	{
		return "Dagger";
	}

	
	//Override
	public string getImageDrawable()
	{
		return "pickaxe";
	}
	
	//Override
	public int getBaseGoldValue()
	{
		return 40;
	}

	//Override
	public int getWeight()
	{
		return 1;
	}
}
