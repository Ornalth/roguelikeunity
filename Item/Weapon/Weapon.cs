




public abstract class Weapon : Equipment
{
	protected Weapon(int durability)
	{
		super(durability);
	}
	protected int attack;
	protected int speed = 1;
	protected int range;
	public int getAttack()
	{
		return attack;
	}
	abstract public int getAttack(Unit unit); // Vs unit
	abstract public int getSpeed();
	
	public int getRange()
	{
		return range;
	}
	
	abstract public bool shouldPierce(Unit unit);
	

}
