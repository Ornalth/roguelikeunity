



public class Axe : Weapon
{
	
	public Axe(int durability)
	{
		super(durability);
		attack = 10;
		speed = 1;
		range = 1;
	}

	//Override
	public int getAttack(Unit unit)
	{

		return attack;
	}

	//Override
	public int getSpeed()
	{
		return speed;
	}

	//Override
	public bool shouldPierce(Unit unit)
	{
		return false;
	}

	//Override
	public string getStoreText()
	{
		return "Axe";
	}

	
	//Override
	public string getImageDrawable()
	{
		return "pickaxe";
	}

	//Override
	public int getBaseGoldValue()
	{
		return 75;
	}
	

	//Override
	public int getWeight()
	{
		return 5;
	}
}
