



public class Pickaxe : Weapon
{
	
	public Pickaxe(int durability)
	{
		super(durability);
		attack = 8;
		speed = 2;
		range = 1;
	}

	//Override
	public int getAttack(Unit unit)
	{

		return attack;
	}

	//Override
	public int getSpeed()
	{
		return speed;
	}

	//Override
	public bool shouldPierce(Unit unit)
	{
		return false;
	}

	//Override
	public string getStoreText()
	{
		return "Pickaxe";
	}

	
	
	//Override
	public string getImageDrawable()
	{
		return "pickaxe";
	}

	//Override
	public int getBaseGoldValue()
	{
		return 120;
	}
	

	//Override
	public int getWeight()
	{
		return 3;
	}
}
