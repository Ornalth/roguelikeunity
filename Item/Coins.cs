



public class Coins : StackableItem
{
	int value;

	public Coins()
	{
		super();
		this.value = rand.nextInt(200) + 5;

	}

	public Coins(int gold)
	{
		value = gold;
	}

	//Override
	public bool shouldDisappear()
	{
		return true;
	}

	//Override
	public void use(Player origin)
	{
		origin.addGold(value);
	}

	//Override
	public bool join(StackableItem item)
	{
		if (canJoin(item))
		{
			this.value += ((Coins) item).value;
			return true;
		}
		return false;
	}

	//Override
	public string getStoreText()
	{
		return "" + this.value + " Gold";
	}

	//Override
	public string getImageDrawable()
	{
		return "coins";
	}

	//Override
	public string getName()
	{
		return "Coins";
	}

	//Override
	public int getBaseGoldValue()
	{
		return value;
	}

	//Override
	public bool canJoin(StackableItem item)
	{
		if (item is Coins)
		{
			return true;
		}
		return false;
	}

	//Override
	public Item deepCopy()
	{
		Coins coins = new Coins();
		coins.value = this.value;
		coins.stack = this.stack;
		return coins;
	}

	//Override
	public int getWeight()
	{
		return 0;
	}


}
