



public class Utils
{
	public static int max(int a, int b)
	{
		if (a > b)
			return a;
		return b;
	}

	public static int getStatusEffectIndex(List<StatusEffect> statuses,
			StatusEffect effect)
	{
		int index = 0;
		foreach (StatusEffect eff in statuses)
		{
			if (eff.status == effect.status)
			{
				return index;
			}
			index++;
		}
		return -1;
	}
}
