





public enum UnitModifier
{
	NONE("", 1,1,1,1,1, 0, 1),
	SLOW("Slow", 1,1,0.8,1,1,0, 0.9), 
	FAST("Fast", 1,1,1,1.2,1,0, 1.1),  
	WEAK("Weak", 0.8,1,1,1,1,0, 0.8),  
	STRONG("Strong", 1.2,1,1,1,1,0, 1.2),  
	ENIGMA("???", 1,1,1,1,1,0, 1);

	private string name;
	private static UnitModifier[] modifiers = values();
	private static Random rand = new Random();
	private Modifier modifier;

	private UnitModifier(string s, double attackModifier, double defenseModifier,
			double AgiModifier, double intModifier, double charmModifier,
			int sightModifier, double expModifier)
	{
		this.name = s;
		modifier = new Modifier(attackModifier, defenseModifier,
				AgiModifier, intModifier, charmModifier, sightModifier, expModifier);
	}

	public string getName()
	{
		return name;
	}

	public static UnitModifier getRandomModifier()
	{
		return modifiers[rand.nextInt(modifiers.length)];
	}

	public double getAttackModifier()
	{

		return modifier.attackModifier;

	}

	public double getDefenseModifier()
	{

		return modifier.defenseModifier;

	}

	public double getAgiModifier()
	{

		return modifier.AgiModifier;

	}

	public double getIntModifier()
	{

		return modifier.intModifier;

	}

	public double getCharmModifier()
	{

		return modifier.charmModifier;

	}
	
	public int getSightModifier()
	{

		return modifier.sightModifier;

	}
	
	public double getExpModifier()
	{

		return modifier.expModifier;

	}
}
