





public class EnemyFactory
{
	private static Random rand = new Random();

	public static Enemy createEnemy(Position pos)
	{
		Enemy enemy;
		switch (rand.nextInt(3))
		{
			case 0:
			{
				enemy = new Slime(pos);
				break;
			}
			case 1:
			{
				enemy = new DarkSlime(pos);
				break;
			}
			case 2:
			{
				enemy = new BurningSlime(pos);
				break;
			}
			default:

				enemy = new Slime(pos);
				break;

		}
		enemy.setModifier(UnitModifier.getRandomModifier());
		return enemy;

	}

	public static Enemy createEnemy(Position pos, int playerLevel)
	{

		Enemy enemy = createEnemy(pos);
		enemy.addLevels(playerLevel - 1);
		return enemy;
	}

}
