






public abstract class Enemy : Unit{
	//TODO gold
	bool aggro = false;
	int range;
	
	protected Enemy(Position pos)
	{
		this.row = pos.row;
		this.col = pos.col;
	}
	
	//TODO armor
	//Override
	public int takeDamage(Unit source, int baseDamage)
	{
		int damage = (int) (baseDamage - modifier.getDefenseModifier() * this.getDef());
		hp -= damage;
		return damage;
	}
	
	//TODO armor
	//Override
	public int takeDamage(EVENT ev, int baseDamage)
	{
		int damage = (int) (baseDamage - modifier.getDefenseModifier() * this.getDef());
		hp -= damage;
		return damage;
	}

	public bool isAggro()
	{
		return aggro;
	}

	public void setAggro(bool aggro)
	{
		this.aggro = aggro;
	}

	public int getRange()
	{
		return range;
	}

	public void setRange(int range)
	{
		this.range = range;
	}

	abstract public void makeMove();
	
	protected bool hasVision(Unit unit)
	{
		return getVision() >= Position.getOtherTypeDistance(getPosition(), unit.getPosition());
	}

	protected bool canAttack(Unit unit)
	{
		int distance = Position.getOtherTypeDistance(getPosition(), unit.getPosition());
		Debug.Log(distance);
		return getRange() >= distance; 
	}

	public void addLevels(int numLevels)
	{
		for (int i = 0; i < numLevels; i++)
		{
			gainStats();
		}
	}
	
	

}
