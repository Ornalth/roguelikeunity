







public class UnitFactory
{
	private static Random rand = new Random();
	public static Unit createUnit(Position pos)
	{
		if (rand.nextInt() % 2 == 0)
		{
			return EnemyFactory.createEnemy(pos);
		}
		else
		{
			return NPCFactory.createNPC(pos);
		}
	}
}
