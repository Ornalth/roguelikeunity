







public class Slime : Enemy
{
	private static Random rand = new Random();
	protected Slime(Position pos)
	{
		super(pos);
		vision = 3;
		//speed = 1;
		level = 1;
		maxHP = 10;
		maxMP = 0;
		str = 5;
		agi = 1;
		charm = 1;
		intellect = 1;
		def = 0;
		range = 1;
		aggro = true;
		//TODO random item;
		
	}

	//Override
	public void makeMove()
	{
		Board board = Board.getInstance();
		Player player = board.getPlayer();
		// HACK for now
		for (int i = 0; i < 1; i++)
		{
			if (this.hasVision(player))
			{
				if (this.canAttack(player))
				{

					board.unitRequestAttack(this, board.getPlayerTile());
				}
				else
				{
					board.unitRequestMoveTowardsPlayer(this, 1); // 1 -- speed
				}
			}
			else
			{
				board.unitRequestMove(this, 1); // 1 -- speed
			}
		}
	}

	//Override
	public string getDrawableName()
	{
		return "slime";
	}

	//Override
	public int getExpValue()
	{
		return 5;
	}

	//Override
	public int getNextLevelExp()
	{
		return 9999;
	}

	//Override
	protected void gainStats()
	{
		maxHP+= 5;
		hp+=5;
		str += 2;
		level++;
	}

	//Override
	public string getName()
	{
		return modifier.getName() + " Slime";
	}

}
