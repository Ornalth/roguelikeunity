










public abstract class MerchantNPC : NPC
{
	Random rand = new Random();

	MerchantNPC(Position pos)
	{
		super(pos);
		createInitialItems();
	}

	

	//Override
	public bool canTrade()
	{
		return true;
	}

	abstract protected void createInitialItems();
	abstract public Offer getOfferForItem(Item item, int charm);
	abstract public List<Offer> getOffers(int charm);
	abstract public List<Offer> getOfferForItems(List<Item> items, int charm);

	public void addBoughtOffers(List<Offer> offers)
	{
		foreach (Offer offer in offers)
		{
			addItemToInventory(offer.item);
		}
	}



	public void removeSoldOffers(List<Offer> offers)
	{
		foreach (Offer offer in offers)
		{
			if (offer.item is StackableItem)
			{
				List<Item> remove = new List<Item>();
				foreach (Item item in items)
				{
					if (item is StackableItem)
					{
						StackableItem stack = (StackableItem) item;
						if (stack.canJoin((StackableItem) offer.item))
						{
							int normalStackCount = stack.getStackAmount();
							int sellStackCount = ((StackableItem)offer.item).getStackAmount();
							if (normalStackCount == 0)
							{
								remove.Add(item);
							}
							break;
						}
					}
				}
				items.removeAll(remove);
			
			}
			else
			{
				items.remove(offer.item);
			}
		}
	}

}
