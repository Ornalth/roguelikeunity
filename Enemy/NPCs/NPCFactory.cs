






public class NPCFactory
{
	private static Random rand = new Random();

	// TODO
	public static NPC createNPC(Position position)
	{
		switch(rand.nextInt(3))
		{
			case 0:
				return createMerchantNPC(position);
			case 1:
				return createGenericNPC(position);
			case 2:
				return createEventNPC(position);
		}
		return createMerchantNPC(position);
	}

	public static NPC createMerchantNPC(Position position)
	{
		MerchantNPC npc = null;
		switch (rand.nextInt(3))
		{
			case 0:
				npc = new ConsumableMerchantNPC(position);
				break;
			case 1:
				npc = new WeaponMerchantNPC(position);
				break;
			case 2:
				npc = new ArmorMerchantNPC(position);
				break;
		}
		npc.setModifier(UnitModifier.getRandomModifier());
		return npc;
	}

	public static NPC createGenericNPC(Position pos)
	{
		return new OldManNPC(pos);
	}
	
	public static NPC createEventNPC(Position pos)
	{
		return new CactusShrine(pos);
	}
}
