








public class ConsumableMerchantNPC : MerchantNPC
{

	ConsumableMerchantNPC(Position pos)
	{
		super(pos);
	}

	//Override
	public Offer getOfferForItem(Item item, int charm)
	{
		double charmCost = (100 + charm * 2) / 100;
		return new Offer(item, (int) (item.getBaseGoldValue() / 2 * charmCost));
	}

	//Override
	public List<Offer> getOffers(int charm)
	{
		List<Offer> offers = new List<Offer>();
		double charmCost = (100.0 - charm * 5.0) / 100;
		foreach (Item item in items)
		{
			offers.Add(new Offer(item, (int) (item.getBaseGoldValue() * charmCost)));
		}
		return offers;
	}

	//Override
	public bool isFinishedDialog()
	{
		return true;
	}

	//Override
	public void advanceDialog()
	{
		// DO NOTHING?..
	}

	//Override
	public string getCurrentDialogMessage()
	{
		return "BUY FROM ME NOOB";
	}

	//Override
	public string getDrawableName()
	{
		return "consumablemerchant";
	}

	//Override
	protected void createInitialItems()
	{
		int numItems = rand.nextInt(3) + 2;
		for (int i = 0; i < numItems; i++)
		{
			addItemToInventory(ItemFactory.createConsumable());
		}
	}

	//Override
	public string playerDidLeaveMessage()
	{
		return "Player Did leave this NPC!";
	}

	//Override
	public void resetDialog()
	{
		// TODO Auto-generated method stub
		
	}

	//Override
	public List<Offer> getOfferForItems(List<Item> items, int charm)
	{
		List<Offer> offers = new List<Offer>();
		foreach (Item item in items)
		{
			Offer offer = getOfferForItem(item, charm);
			offers.Add(offer);
		}
		return offers;
	}
	
	//Override
	public string getName()
	{
		return modifier.getName() + " Consumable NPC";
	}


}
