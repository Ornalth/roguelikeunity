










public class CactusShrine : NPC , Requester
{

	List<string> options;

	protected CactusShrine(Position pos)
	{
		super(pos);
	}

	//Override
	public bool canTrade()
	{
		return false;
	}

	//Override
	public bool isFinishedDialog()
	{
		return true;
	}

	//Override
	public void advanceDialog()
	{
		
	}

	//Override
	public string getCurrentDialogMessage()
	{
		return "cactus shrine";
	}

	//Override
	public string playerDidLeaveMessage()
	{
		// TODO Auto-generated method stub
		return null;
	}

	//Override
	public void resetDialog()
	{
		// TODO Auto-generated method stub

	}

	//Override
	public string getName()
	{
		return "eventnpc";
	}


	public void interact(Player player)
	{
		int align = player.align.getAlignment(Faith.SLIME);
		if (align > 10)
		{

		}
	}

	/**
	 * override to have text selection options;
	 * @return
	 */
	public List<string> getCurrentOptions()
	{
		options = new List<string>();
		options.Add("Donate");
		options.Add("Pray");
		options.Add("Interact");
		return options;
	}

	public void optionSelected(int index)
	{
		string option = options.get(index);
		if (option.equals("Donate"))
		{
			Board board = Board.getInstance();
			board.requestIntegerValue(this,
					"How much would you like to donate?", board.getPlayer()
							.getGold());
		}
		Board.log("Option selected: " + option);
	}

	//Override
	public void satisfyIntegerValue(int amount)
	{
		donate(amount);
	}

	private void donate(int amount)
	{
		Board board = Board.getInstance();
		Player player = board.getPlayer();
		if (amount > 0)
		{
			Debug.Log("MESSAGE DONATE!");
		}
	}

	//Override
	public string getDrawableName()
	{
		return "crossshrine";
	}

}
