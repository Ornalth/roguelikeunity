








public abstract class NPC : Unit
{

	protected NPC(Position pos)
	{
		this.row = pos.row;
		this.col = pos.col;
	}
	//TODO armor
	//Override
	public int takeDamage(Unit source, int baseDamage)
	{
		int damage = (int) (baseDamage - modifier.getDefenseModifier() * this.getDef());
		hp -= damage;
		return damage;
	}

	//TODO armor
	//Override
	public int takeDamage(EVENT ev, int baseDamage)
	{
		int damage = (int) (baseDamage - modifier.getDefenseModifier() * this.getDef());
		hp -= damage;
		return damage;
	}

	public void makeMove()
	{
		//DO NADA.
	}
	
	
	abstract public bool canTrade();

	abstract public bool isFinishedDialog();

	abstract public void advanceDialog();
	abstract public string getCurrentDialogMessage();
	abstract public string playerDidLeaveMessage();
	
	//Override
	public int getExpValue()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	//Override
	public int getNextLevelExp()
	{
		// TODO Auto-generated method stub
		return 1;
	}

	//Override
	protected void gainStats()
	{
		// TODO Auto-generated method stub
		
	}
	abstract public void resetDialog();
	/**
	 * override to have text selection options;
	 * @return
	 */
	public List<string> getCurrentOptions()
	{
		return null;
		
	}
	/**
	 * OVERRIDE TO DO...
	 * @param storedIndex
	 */
	public void optionSelected(int storedIndex)
	{
		//...
	}
}
