





public class OldManNPC : NPC
{

	List<string> dialogMessages = new List<string>();
	int currentMessage = 0;
	
	OldManNPC(Position pos)
	{
		super(pos);
	}

	//Override
	public string getDrawableName()
	{
		return "genericnpc";
	}

	//Override
	public bool canTrade()
	{
		return false;
	}

	//Override
	public bool isFinishedDialog()
	{
		// TODO Auto-generated method stub
		return false;
	}

	//Override
	public void advanceDialog()
	{
		currentMessage++;
	}

	//Override
	public string getCurrentDialogMessage()
	{
		if (dialogMessages.Count > currentMessage)
		{
			return dialogMessages.get(currentMessage);
		}
		else
		{
			return "WHAT THE";
		}
	}

	//Override
	public string playerDidLeaveMessage()
	{
		return "Player left old man npc.";
	}
	
	//Override
	public void resetDialog()
	{
		// TODO Auto-generated method stub
		
	}
	
	//Override
	public string getName()
	{
		return modifier.getName() + " Old Man NPC";
	}

	

}
