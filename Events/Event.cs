



// LEGEND @ - enemies, E - Incidents, I- items, P - player, B - bosses
public abstract class EVENT {

	protected int row;
	protected int col;
	abstract public string getDrawableName();
	
	abstract public List<string> getOptions();
	abstract public void optionSelected(int index);
}
