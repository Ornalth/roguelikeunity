



public class FaithAlignment
{
	private Dictionary<Faith, Integer> align = new Dictionary<Faith, Integer>();
	
	public FaithAlignment()
	{
		foreach (Faith faith in Enum.GetValues(typeof(Faith)))
		{
			align.put(faith, 0);
		}
	}
	
	public int getAlignment(Faith f)
	{
		return align[f];
	}
	
	public void setAlignment(Faith f,int val)
	{
		align.put(f, val);
	}
	
	public void addAlignment(Faith f,int val)
	{
		align.put(f, align[f] + val);
	}
}
