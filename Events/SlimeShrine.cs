







public class SlimeShrine : EVENT , Requester
{
	List<string> options;
	
	public SlimeShrine (Position pos)
	{
		this.row = pos.row;
		this.col = pos.col;
	}
	
	public void interact(Player player)
	{
		int align = player.align.getAlignment(Faith.SLIME);
		if (align > 10)
		{
			
		}
	}
	
	
	//Override
	public List<string> getOptions()
	{
		options = new List<string>();
		options.Add("Donate");
		options.Add("Pray");
		options.Add("Interact");
		return options;
	}
	
	//Override
	public void optionSelected(int index)
	{
		string option = options.get(index);
		if (option.equals("Donate"))
		{
			Board board = Board.getInstance();
			board.requestIntegerValue(this, "How much would you like to donate?", board.getPlayer().getGold());
		}
	}
	
	//Override
	public void satisfyIntegerValue(int amount)
	{
		donate(amount);
	}
	
	private void donate(int amount)
	{
		Board board =  Board.getInstance();
		Player player = board.getPlayer();
		if (amount > 0)
		{
			
		}
	}
	
	//Override
	public string getDrawableName()
	{
		return "shrine";
	}

}
