



// "." == EMPTY SPOT || ENEMY SPOT || ITEM SPOT ANYTHING YOU WANT.
// "W" == WALL
// "C" == NPC SPOT || ITEM SPOT
// "N" == NPC SPOT
// "D" == ENEMY SPOT || ITEM SPOT
// "E" == ENEMY
// "X" == MUST BE BLANK (DOORS)

public class TileFactory
{
	static Random rand = new Random();

	public static Tile createTile(char tileType, bool random, int row, int col)
	{
		Tile tile = null;
		if (tileType == 'W')
		{
			tile = new Wall();

		}
		else if (tileType == 'D' || tileType == 'E')
		{
			tile = new Dirt();
		}
		else if (tileType == 'C' || tileType == 'N')
		{
			tile = new Tiling();
		}
		else if (tileType == 'X')
		{
			tile = new Door();
		}
		else
		{// TODO
			if (random)
			{
				// Randomly gen tile.
				switch (rand.nextInt(5))
				{
					case 0:
						tile = new Grass();
						break;
					case 1:
						tile = new Mountain();
						break;
					case 2:
						tile = new River();
						break;
					case 3:
						tile = new Forest();
						break;
					case 4:
						tile = new Road();
						break;
					default:
						tile = new Grass();
						break;

				}
				tile = new Grass();
			}
			else
			// TODO
			{
				tile = new Grass();
			}
		}
		tile.row = row;
		tile.col = col;
		return tile;
	}
}
