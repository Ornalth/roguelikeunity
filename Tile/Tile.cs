









// LEGEND @ - enemies, E - Incidents, I- items, P - player, B - bosses

public abstract class Tile {
	protected Unit unit;
	protected EVENT ev;
	List<Item> items = new List<Item>();
	
	public int row;
	public int col;
	
	public EVENT getEvent()
	{
		return ev;
	}
	public void setEvent(EVENT ev)
	{
		this.ev = ev;
	}
	public Unit getUnit()
	{
		return unit;
	}

	public void setUnit(Unit unit2)
	{
		unit = unit2;
		
	}
	
	public void addItem(Item item)
	{
		if (item is StackableItem)
		{
			bool success = false;
			foreach (Item currentItem in items)
			{
				if (currentItem is StackableItem && ((StackableItem)currentItem).join((StackableItem)currentItem))
				{
					success = true;
					break;
				}
			}
			if (!success)
			{
				items.Add(item);
			}
		}
		else
		{
			items.Add(item);
		}
		
	}
	
	public List<Item> getItems()
	{
		return items;
	}
	
	public bool hasUnit()
	{
		return getUnit() != null;
	}
	//abstract public int getMovementCost(Unit unit);
	
	/*
	public string type;
	public string extra;
	public Enemy enemy;
	public Item item;
	public EVENT incident;
	public int visible;
	public Player dude;
	public bool marked;
	List <ImageIcon> tileImage = new List<ImageIcon>();
	// visible 0 = invisible
	// visible 1 = 50% vis
	// visible 2 = completely seen
	public Tile(string t)
	{
		visible = 0;
		extra = ".";
		type = t;
		marked = false;
	}
	void setEmpty()
	{
		marked = false;
		type = ".";
		extra = ".";
		enemy = null;
		item = null;
		incident = null;
		dude = null;
	}
	void setPlayer(Player Player)
	{
		//setEmpty();
		extra = type;
		type = "P";
//		enemy = null;
//		item = null;
//		incident = null;
		dude = Player;
		
	}
	void newEnemy(Player Player)
	{
		if (enemy == null)
		{
			setEmpty();
			type = "@";
			extra = ".";
			enemy = new Enemy(Player);
		}
	}
	void newEnemy(string str, Player Player)
	{
		if (enemy == null)
		{
			setEmpty();
			type = "B";
			extra = ".";
			enemy = new Enemy(str,Player);
		}
	}
	void newItem(Player Player)
	{
		if (item == null)
		{
			setEmpty();
			type = "I";
			extra = ".";
			item = new Item(Player);
		}
	}
	void newIncident(Player Player)
	{
		if (incident == null)
		{
			setEmpty();
			type = "E";
			incident = new EVENT(Player);
		}
	}
	void setExtra(string s)
	{
		extra = s;
	}
	void deleteItem()
	{
		item = null;
		extra = ".";
		//because player has to be on top fo item to pick it up,
		if (type == "I")
			type = ".";
	}
	void deleteEnemy()
	{
		if (enemy.onDeath.contains("PSNF"))
		{
			//poison field is type 10, duration = enemy.lvl + 1
			incident = new EVENT(10, enemy.lvl + 1);
			//below changes type to extra
			extra = "E";
		}
		enemy = null;
		marked = false;
		//incase enemy was on top of item?
		type = extra;
		extra = ".";
		
	}
	void deleteIncident()
	{
		incident = null;
		//incase enemy was on top of item?
		type = extra;
		extra = ".";
		
	}
	void playerLeft()
	{
		type = extra;
		extra = ".";
	}
	string getInfo()
	{
		//TODO incidents
		if (isVisible())
		{
			if (type == "@" || type == "B")
				return enemy.getInfo();
			else if (type == "I")
				return item.getInfo();
			else if ( type == "E")
				return incident.getInfo();
			else if (type == "P")
				return "You are above a type: " + this.extra;
			else if (type == "#")
				return "You see a wall.";
		}
		return "Not yet coded object?";
	}
	bool isVisible()
	{
		return visible == 2;
	}
	void setVisible(int i)
	{
		if (i >= 2)
			visible = 2;
		if (i < 0 )
			visible = 0;
		else 
			visible = i;
	}*/
	


	public Position getPosition()
	{
		Position p = new Position();
		p.row = row;
		p.col = col;
		return p;
	}
	public abstract string getDrawableName();
	public bool hasItems()
	{
		return items.Count > 0;
	}
	
	public bool hasEvent()
	{
		return ev != null;
	}
	public void addItems(List<Item> items2)
	{
		foreach (Item item in items2)
		{
			addItem(item);
		}
	}

}
