





public class Position
{
	public int col;
	public int row;
	public int movesLeft = 0;
	public Position parent;

	public Position()
	{

	}

	public Position(int row, int col)
	{
		this.col = col;
		this.row = row;
	}

	public Position(int row, int col, int movesLeft)
	{
		this.col = col;
		this.row = row;
		this.movesLeft = movesLeft;
	}

	//Override
	public bool equals(Object obj)
	{
		if (obj is Position)
		{
			Position pos = (Position) obj;
			return pos.col == this.col && pos.row == this.row;
		}

		return super.equals(obj);
	}

	//Override
	public int hashCode()
	{
		int hash = 2 + row / 3 + col * 23 / 7;
		return hash;
	}

	//Override
	public string tostring()
	{
		return "Row: " + row + " Col: " + col;
	}


	public static int getManhattanDistance(Position unitPos, Position targetPos)
	{
		int distance = 0;

		distance += Math.abs(unitPos.col - targetPos.col);
		distance += Math.abs(unitPos.row - targetPos.row);
		return distance;
	}
	
	public static int getOtherTypeDistance(Position unitPos, Position targetPos)
	{
		int changeX = Math.abs(unitPos.col - targetPos.col);
		int changeY = Math.abs(unitPos.row - targetPos.row);
		return Math.max(changeX, changeY);
	}
	
	


	public List<Position> getNeighbours()
	{

		List<Position> pos = new List<Position>();
		
		//RIGHT
		if (col + 1 < Constants.width)
		{
			pos.Add(new Position(row, col + 1));
		}
		// Left
		if (col - 1 >= 0)
		{
			pos.Add(new Position(row, col - 1));
		}
		// UP

		if (row + 1 < Constants.height)
		{
			pos.Add(new Position(row + 1, col));
		}
		// DOWN
		if (row - 1 >= 0)
		{
			pos.Add(new Position(row - 1, col));
		}
		
		//RIGHT && UP
		if (col + 1 < Constants.width && row + 1 < Constants.height)
		{
			pos.Add(new Position(row + 1, col + 1));
		}
		//LEFT && UP
		if (col - 1 >= 0 && row + 1 < Constants.height)
		{
			pos.Add(new Position(row + 1, col - 1));
		}
		
		//RIGHT && DOWN
		if (col + 1 < Constants.width && row - 1 >= 0)
		{
			pos.Add(new Position(row - 1, col + 1));
		}
		// Left and down
		if (col - 1 >= 0 && row - 1 >= 0)
		{
			pos.Add(new Position(row - 1, col - 1));
		}
	

		
		return pos;

	}

	public Position modifyByDirection(Direction dir)
	{
		return new Position(row + Direction.getChangeY(dir), col + Direction.getChangeX(dir));
	}

	public Position Add(int row, int col)
	{
		return new Position(this.row + row, this.col + col);
	}


	
}
